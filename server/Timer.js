'use strict';
var schedule = require('node-schedule');

var Timer = (function () {

    var Timer = function () {
        this.rollCooldown = 10 * 1000;
        this.nextToRoll = Date.now() + this.rollCooldown;
    };

    Timer.getNext = function() {
        return this.nextToRoll;
    };

    Timer.startSchedule = function (timer) {
        (function updateNext() {
            timer.nextToRoll = Date.now() + timer.rollCooldown;
            schedule.scheduleJob(timer.nextToRoll, () => {
                console.log(timer.nextToRoll);
                updateNext();
            })
        })();
        return timer;
    };
    return Timer;
})();

module.exports = Timer;