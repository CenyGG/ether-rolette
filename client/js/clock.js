function getTimeRemaining(endtime) {
    var t = endtime - Date.now();
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

function initializeClock(id, endtime) {
    var minutesSpan = $('.minutes');
    var secondsSpan = $('.seconds');

    function updateClock() {
        var t = getTimeRemaining(endtime);

        if (t.total > 0) {
            minutesSpan.text(('0' + t.minutes).slice(-2));
            secondsSpan.text(('0' + t.seconds).slice(-2));
        } else {
            clearInterval(timeInterval);
            resetTimer();
        }
    }

    updateClock();
    var timeInterval = setInterval(updateClock, 1000);
}

function resetTimer() {
    $.ajax({
        type: "GET",
        url: "/api/next"
    }).done(function (data) {
        console.log(data);
        initializeClock('clockdiv', data);
    });
}