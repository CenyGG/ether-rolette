//todo for test to remove

var EthUtils = (function () {

    var EthUtils = function () {
    };

    EthUtils.getTransactionsByAccount = function (myaccount, startBlockNumber, endBlockNumber) {
        if (endBlockNumber == null) {
            web3.eth.getBlockNumber((err, lastBlock) => {
                if (err != null) {
                    console.log(err);
                    return err;
                } else {
                    this.getTransactionsByAccount(myaccount, startBlockNumber, lastBlock)
                }
            });
            return;
        }
        if (startBlockNumber == null) {
            startBlockNumber = endBlockNumber - 300000;
            console.log("Using startBlockNumber: " + startBlockNumber);
        }
        console.log("Searching for transactions to/from account \"" + myaccount + "\" within blocks " + startBlockNumber + " and " + endBlockNumber);

        for (let i = startBlockNumber; i <= endBlockNumber; i++) {
            if (i % 10000 === 0) {
                console.log("Searching block " + i);
            }
            web3.eth.getBlock(i, (err, block) => {
                if (err == null && block != null && block.transactions != null) {
                    block.transactions.forEach(function (txHash) {
                        web3.eth.getTransaction(txHash, (err, tx) => {
                            if (err == null && tx != null) {
                                if (myaccount === "*" || myaccount === tx.from || myaccount === tx.to) {
                                    console.log("  tx hash          : " + tx.hash + "\n"
                                        + "   nonce           : " + tx.nonce + "\n"
                                        + "   blockHash       : " + tx.blockHash + "\n"
                                        + "   blockNumber     : " + tx.blockNumber + "\n"
                                        + "   transactionIndex: " + tx.transactionIndex + "\n"
                                        + "   from            : " + tx.from + "\n"
                                        + "   to              : " + tx.to + "\n"
                                        + "   value           : " + tx.value + "\n"
                                        + "   time            : " + block.timestamp + " " + new Date(block.timestamp * 1000).toGMTString() + "\n"
                                        + "   gasPrice        : " + tx.gasPrice + "\n"
                                        + "   gas             : " + tx.gas + "\n"
                                        + "   input           : " + tx.input);
                                }
                            } else {
                                // err != null ? console.log(err)
                                //     : console.log("Transaction " + txHash + " has been included into BlockNo #" + block + " and will be reflected in a shortwhile");
                            }

                        });


                    })
                }
            });
        }
    };

    return EthUtils;
})();

