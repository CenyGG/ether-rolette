const express = require('express');
const app = express();

const Timer = require('./server/Timer.js');
const timer = new Timer();
Timer.startSchedule(timer);


const path = require("path");
app.use(express.static("client"));

app.route('/')
    .get(function (req, res) {
        res.sendFile(path.join("index.html"))
    });

app.route('/api/next')
    .get(function (req, res) {
        res.send("" + timer.nextToRoll);
    });

app.listen(3000, () => console.log('Example app listening on port 3000!'));
